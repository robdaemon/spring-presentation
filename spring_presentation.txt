# Spring Framework
## Or, how we build Java services for fun and profit

Rob Roland

May 12, 2014

# Agenda

* What is Spring?
* Dependency Injection
* Web MVC

---

## Spring Framework

* Spring is a big, complex application framework but it's modular
	+ Dependency Injection
	+ Web MVC
	+ Pick and choose your adventure

* Spring has other components we're not covering today
	+ Data Access
	+ Aspect Oriented Programming
	+ Testing

---

## Dependency Injection

* It's a design pattern
	+ The term was coined by Martin Fowler
* Allows loose coupling between a dependency and the client of that dependency
	+ Separates the construction of dependencies from their usage
* Makes unit testing easier

---

## Use cases for dependency injection

* You have a different database in each environment
	+ You use HSQLDB during development and PostgreSQL in production
* You have multiple data providers for the same data
	+ We gather Twitter data from three sources: Twitter API, Gnip and DataSift, but it's all Twitter data, and it all gets stored the same way.

---

## How does that work?

Consider this example:

```java
public class OrderController {
	private OrderDAO myDAO = null;

	public OrderController() {
		this.orderDAO = new OrderDAO("mysql", "localhost", 3306, 
			"orderdb", "myusername", "mypassword");
	}

	public ModelAndView index(HttpServletRequest request) {
		List<Order> orders = this.orderDAO.getOrders();

		return new ModelAndView("orders", orders);
	}
}
```

---

## Okay, what's wrong with that?

* Database connection is hardcoded
* Database connection is (potentially) established as soon as the controller is instantiated
* Cannot unit test this without connecting to the database, making it an integration test

---

## A better way - the configuration

```java
@Configuration
@Profile("mysql")
public class DatabaseConfiguration {
	@Value("${database.host}")
	private String databaseHost;
	@Value("${database.port}")
	private int databasePort;
	@Value("${database.dbname}")
	private String databaseName;
	@Value("${database.username}")
	private String username;
	@Value("${database.password}")
	private String password;
	@Value("${database.max_connections}")
	private int maxConnections;
```

---

## A better way - the configuration

```java
	@Bean
	public DataSource dataSource() {
        DriverAdapterCPDS connectionPool = new DriverAdapterCPDS();
        connectionPool.setDriver("com.mysql.jdbc.Driver");
        connectionPool.setUrl(
        	String.format("jdbc:mysql://%s:%d/%s", 
        		databaseHost, databasePort, databaseName));

        SharedPoolDataSource dataSource = new SharedPoolDataSource();
        dataSource.setConnectionPoolDataSource(connectionPool);
        dataSource.setMaxTotal(maxConnections);

        return dataSource;		
	}
}
```

---

## A better way - the DAO

```java
@Component
public class DatabaseOrderServiceImpl implements OrderService {
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void init(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Order> getOrders() {
		return this.jdbcTemplate.queryForList("SELECT * FROM orders");
	}
}
```

---

## A better way - What if we use an external service?

```java
@Component
public class RESTfulOrderServiceImpl implements OrderService {
	@Value("orders.base_url")
	private String orderServerBaseUrl;

	@Override
	public List<Order> getOrders() {
		RestTemplate restTemplate = new RestTemplate();

		String url = String.format("%s/1/orders", this.orderServerBaseUrl);

		return restTemplate.getForObject(url, List.<Orders>class);
	}
}
```

---

## A better way - the controller

```java
@Controller
public class OrderController {
	@Autowired
	private OrderService orderService;

	public ModelAndView index(HttpServletRequest request) {
		List<Order> orders = this.orderService.getOrders();

		return new ModelAndView("orders", orders);
	}
}
```

---

## Behind the scenes...

Everything known by the container is a "bean" - each bean has a identifier, basically kept in a big Map.

Upon request, you either get a new instance of a bean, if it's scoped "prototype" or, a singleton instance of a bean, the default scope.

---

## How does this all happen?

Spring calls it an ApplicationContext. It's the "man behind the curtain" of your application.

You can define this in a few ways:

---

## XML, using Setter injection

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.springframework.org/schema/beans
      http://www.springframework.org/schema/beans/spring-beans.xsd">

  <bean id="petStore" 
    class="com.petstore.services.PetStoreServiceImpl">
    <property name="accountDao" ref="accountDao"/>
    <property name="itemDao" ref="itemDao"/>
  </bean>
</beans>
```

---

## Setter injection

```java
public class PetStoreServiceImpl implements PetStore {
	private AccountDao accountDao;
	private ItemDao itemDao;

	public void setAccountDao(AccountDao accountDao) {
		this.accountDao = accountDao;
	}

	public void setItemDao(ItemDao itemDao) {
		this.itemDao = itemDao;
	}
}
```

---

## XML, using Constructor injection

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.springframework.org/schema/beans
      http://www.springframework.org/schema/beans/spring-beans.xsd">

  <bean id="petStore" 
    class="com.petstore.services.PetStoreServiceImpl">
    <constructor-arg ref="accountDao"/>
    <constructor-arg ref="itemDao"/>
  </bean>
</beans>
```

---

## Constructor injection

```java
public class PetStoreServiceImpl implements PetStore {
	private AccountDao accountDao;
	private ItemDao itemDao;

	public PetStoreServiceImpl(AccountDao accountDao, ItemDao itemDao) {
		this.accountDao = accountDao;
		this.itemDao = itemDao;	
	}
}
```

---

## XML bootstrapping

```java
public class App {
	public static void main(String[] args) {
		ApplicationContext context =
    		new ClassPathXmlApplicationContext(
    			new String[] {"application.xml"});
	}
}
```

---

## Java / Annotations

```java
@Component
public class PetStoreServiceImpl implements PetStore {
	@Autowired
	private AccountDao accountDao;
	@Autowired
	private ItemDao itemDao;

	...
}
```

---

## Annotation bootstrapping

```java
@Configuration
@ComponentScan({"com.petstore"})
public class AppConfig {
	
}

public class App {
	public static void main(String[] args) {
		ApplicationContext ctx = 
			new AnnotationConfigApplicationContext(AppConfig.class);
	}
}
```

---

## No one uses XML on new projects anymore

These XML files get unwieldy. I've seen them over 5,000 lines long!

You can break them apart and use imports, but it's still a lot of XML.

---

## Constructor injection vs Setter injection

I prefer setter injection.

Constructor args are positional. You have to know the order of the parameters, and it
makes for some nasty Java code. Think about if your bean has 10 dependencies!

This conversation usually devolves to a near religious debate...

---

## Now, realize you probably won't use either.

The annotation-based configuration does not need setters! It works via reflection, and
creates an instance of the bean, and sets the values on the new instance.

In many cases, I still create setters for unit testing purposes.

---

## Interfaces, interfaces, interfaces

One of the most important things to take away here, from all the
examples you've seen. Coding to interfaces:

* Enables you to replace implementations via configuration
* Enables you to mock implementations during testing

---

# Web MVC

---

## Full MVC implementation

* Controllers, Models, Views, Validators, all independent
* Runs in the major servlet containers - Jetty, Tomcat
* Lots of supported template engines
	+ JSP
	+ Velocity
* Many different MessageConverters available to return more than just HTML
	+ Jackson (JSON) - we use this
	+ Protocol Buffers
	+ JAXB XML
* Makes for excellent RESTful APIs
	+ This is how we use it

---

## Request flow

![Request flow](images/mvc.png)

---

## Example controller

Let's use Trident as an example

[TridentController2](https://github.com/simplymeasured/trident-web/blob/master/src/main/java/com/simplymeasured/trident/web/controllers/TridentController2.java)

---

## Class level annotations

```java
@Controller
@RequestMapping(value = "2/")
public class TridentController2 {
}
```

---

## Class level annotations

* @Controller

	This tells Spring we have a Web MVC controller. It registers this controller within the DispatcherServlet. This is
	a singleton controller.

* @RequestMapping

	This tells Spring which URL to hang this controller on.

---

## Member variable annotations

```java
	@Autowired
	protected StatsService statsService;
	@Autowired
	protected Server server;
```

---

## Member variable annotations

* @Autowired

	This causes Spring to automatically inject an instance of this interface.

---

## Method level annotations

A simple endpoint:

```java
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "index";
    }
```

---

## Method level annotations

* @RequestMapping(value = "/", method = RequestMethod.GET)

	Respond to HTTP GET requests on 2/ - combines the class-level @RequestMapping annotation with this method.

* return "index"

	Tells the DispatcherServlet to look for a view file named "index", through the series of view resolvers.

---

## A more complex method

```java
    @RequestMapping(value = "/{customerId}/dashboards/{tridentId}/addByFile", 
    	method = RequestMethod.POST,
        produces = "application/json; charset=UTF-8")
    @ResponseBody
    public UploadWorkbookResponse uploadWorkbook(
    		@PathVariable String customerId, 
    		@PathVariable String tridentId,
            @RequestParam("uploadedFile") MultipartFile file) 
            throws IOException {
        InputStream in = file.getInputStream();
        Server.SubmissionRecord sr = 
	        server.createMultiSheetDashboardVersion(
	        	in, file.getOriginalFilename(), customerId, tridentId);
		in.close();
        UploadWorkbookResponse response = new UploadWorkbookResponse();

        response.setCustomerId(customerId);
        response.setDashboardId(sr.dashboardId);
        response.setVersion(sr.version);
    	
        return response;
    }  
```

---

## Break this down a bit

* @RequestMapping(...)
	+ This endpoint uses path-level variables - so, it would respond to /sm_1/dashboards/c0ffeef00d/addByFile
	+ It only accepts POST
	+ It returns JSON, with a UTF-8 character set

* @ResponseBody
	+ The return value from this function is exactly what gets put in the method response.
	+ UploadWorkbookResponse gets converted to JSON

---

## Break this down a bit

* @PathVariable
	+ Maps the {customerId} part of the URL into a function call parameter at request time

* @RequestParam
	+ Maps the uploadedFile parameter from the request body into a MultipartFile object
	+ Tomcat saves this to disk as the request is dispatched, and this function reads it
	  as an InputStream from the disk.

---

## What have we built using Spring so far?

* Trident
* Data Access Service
* Apprentice
* Report Coordinator
* Report Workers
* Graph Editor (Mason)

---

## Starting a new project

You want to use [Spring Boot](http://projects.spring.io/spring-boot/)

We're using it now, and we'll have a separate session covering Spring Boot itself.

---

## Alternatives in the Java world

* J2EE
* Google Guice
* Dropwizard

---

## J2EE

J2EE was, historically, a mess of XML, complicated Local or Remote interfaces, and bad
performance.

Over the years, J2EE has evolved, mostly from learning from Spring. It now supports annotations,
removed most of the XML and is faster.

Spring started as a means to make working with J2EE easier.

---

## Google Guice

Guice is very similar to Spring. It uses annotations for injection, has abstractions for interacting
with a database, but does not contain a MVC framework. It's fast, but mostly built for Google's use-cases.

---

## Dropwizard

Dropwizard started at Yammer. It's a very easy to use framework, built for web services. It relies on
good, standard Java frameworks. It uses Jersey for REST, Jackson for JSON marshalling, Jetty as a web server,
and integrates with an incredibly powerful metrics library.

We prototyped it for our use, but it wasn't as feature complete as Spring and didn't fully meet our needs.

---

## This is not the end

There's a lot more Spring we use and haven't covered:

* Data Access
* Transaction Management
* AMQP
* Boot

---

## Questions?