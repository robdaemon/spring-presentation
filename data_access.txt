## Data Access

* Easy transaction management
* DAOs
* Coding to the JDBC API is painful
* Tight coupling to ORMs is also a smell

---

# Aspect Oriented Programming

## aka Java's Black Magic

---

## Transaction management

* Global transactions
	+ Example: you need to coordinate a transaction across your message queue and your database
* Declarative
	+ Use an annotation instead of try { ... } catch { ... } blocks
* Multiple transaction backends
	+ JTA (Java Transaction API)
	+ Hibernate
	+ JMS (Java Message Service)
	+ ...

---

# Declarative transactions

---

## The old way

```
public void updateDatabase() throws Exception {
	UserTransaction tx = transactionManager.begin();
	try {
		db.updateValue();
	} catch(Exception ex) {
		tx.rollback();
		throw ex;
	}

	tx.commit();
}
```

---

## The new way

```
@Transactional
public void updateDatabase() throws Exception {
	db.updateValue();
}
```

---

## How does that work, anyhow?

Aspects!
